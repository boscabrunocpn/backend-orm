
import { INTEGER, STRING } from "sequelize";
import db  from '../database-conection.js'


 const Empresa = db.define('empresas', {
        id: {
          type: INTEGER,
          primaryKey: true
        },
        nombre: STRING,
        cuit: STRING
      }, {
        timestamps:false
      });

export default Empresa;

