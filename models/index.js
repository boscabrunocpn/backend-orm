import Empresa from "./Empresa.js";
import Sucursal from "./Sucursal.js";

Empresa.hasMany(Sucursal, {
    foreignKey:"empresa_id",
    as: "Sucursal"
})



Sucursal.belongsTo ( Empresa, {
    foreignKey:'id_empresa',
    as:'empresa'
})



export {Sucursal, Empresa} 