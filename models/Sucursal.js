import { INTEGER, STRING } from "sequelize";
import db from '../database-conection.js';

export const Sucursal = db.define('sucursales',  {
        id: {
          type: INTEGER,
          primaryKey: true
        },
        nombre: STRING,
        direccion: STRING
         
      },  {
        timestamps:false
      }
);
      


export default Sucursal;