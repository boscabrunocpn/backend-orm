import Empresa from "../models/Empresa.js";

export const createEmpresa = (request,response) => {
    const body = request.body;

    if (body.nombre == null) {
        response
        .status (400)
        .send ({ 
            mensaje: 'El nombre es obligatorio'
         });

         return;
   }
   const empresa = {
       nombre: body.nombre,
       cuit: body.cuit
   };
   Empresa.create ( empresa)
        .then (data => {
            response.send(data);
        })
        .catch(error => {
            response
                .status(500)
                .send ({
                    mensaje: "Algo salio mal"
                })
        })

};

export const getAllEmpresa = (request,response) => {
    Empresa.findAll ()
        .then (data => {
            response.send(data)
        })
        .catch(error =>{
            response
            .status(500)
            .send ({
                mensaje: "Algo salio mal"
            })
        })

};

export const getByIdEmpresa = (request,response) => {
    const idEmpresa = request.query.id;
    Empresa.findOne ({where: { id : idEmpresa}})
    .then (data => {
        if (data) {
            response.send(data)
        }
        else {
            response
            .status (404)
            .send ({
                    mensaje: "La Empresa no existe"
            })
        }
    })
    .catch(error =>{
        response
        .status(500)
        .send ({
            mensaje: "Algo salio mal"
        })
    })
};

export const updateByIdEmpresa = (request,response) => {
    const empresaId = request.params.id;
    if (!empresaId || isNaN(empresaId)) {
        response
            .status(400)
            .send({
                mensaje: 'El id es invalido'
            });
            return;

    }
    const body = request.body;
    if (typeof body.nombre != "string" && typeof body.cuit != "string") {
        response
            .status(400)
            .send({
                mensaje: "El nombre y/o cuit no es un String"
            });
            return; 
    }

    const empresa = {}

    if (body.nombre) {
        empresa.nombre = body.nombre;
    }
    
    if (body.cuit) {
        empresa.cuit = body.cuit;
    }

    const filtro = {
        where: {
            id: empresaId
        }
    }

    Empresa.update(empresa,filtro)
        .then (() => {
            mensaje: "El registro se actualizo con exito"
        })
        .catch(() => {
            response
                .status(400)
                .send({
                    mensaje: error.message || 'Algo salio mal con la base de datos'
                })
        })

}   