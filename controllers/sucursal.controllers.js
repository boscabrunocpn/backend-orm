import {Sucursal} from "../models/index.js";

export const createSucursal = (request,response) => {
    const body = request.body;

    if (body.nombre == null) {
        response
        .status (400)
        .send ({ 
            mensaje: 'El nombre es obligatorio'
         });

         return;
   }

   if (body.direccion == null) {
    response
    .status (400)
    .send ({ 
        mensaje: 'La Direccion es obligatorio'
     });

     return;
    }

    if (body.id_empresa == null) {
        response
        .status (400)
        .send ({ 
            mensaje: 'La id_empresa es obligatorio'
         });
    
         return;
        }

   const sucursal = {
       nombre: body.nombre,
       direccion: body.direccion,
       id_empresa: body.id_empresa
   };
   Sucursal.create ( sucursal)
        .then (data => {
            response.send(data);
        })
        .catch(error => {
            response
                .status(500)
                .send ({
                    mensaje: "Algo salio mal"
                })
        })

};

export const getAllSucursal = (request,response) => {
    Sucursal.findAll({include: ['empresa']})
        .then (data => {
            response.send(data)
        })
        .catch(error =>{
            response
            .status(500)
            .send ({
                mensaje: "Algo salio mal"
            })
        })

};

export const getByIdSucursal = (request,response) => {
    const idSucursal = request.query.id;
    Sucursal.findOne ({where: { id : idSucursal}})
    .then (data => {
        if (data) {
            response.send(data)
        }
        else {
            response
            .status (404)
            .send ({
                    mensaje: "La Sucursal no existe"
            })
        }
    })
    .catch(error =>{
        response
        .status(500)
        .send ({
            mensaje: "Algo salio mal"
        })
    })
};

export const updateByIdSucursal = (request,response) => {
    const empresaId = request.params.id;
    if (!empresaId || isNaN(empresaId)) {
        response
            .status(400)
            .send({
                mensaje: 'El id es invalido'
            });
            return;

    }
    const body = request.body;
    if (typeof body.nombre != "string" && typeof body.cuit != "string") {
        response
            .status(400)
            .send({
                mensaje: "El nombre y/o cuit no es un String"
            });
            return; 
    }

    const empresa = {}

    if (body.nombre) {
        empresa.nombre = body.nombre;
    }
    
    if (body.cuit) {
        empresa.cuit = body.cuit;
    }

    const filtro = {
        where: {
            id: empresaId
        }
    }

    Sucursal.update(empresa,filtro)
        .then (() => {
            mensaje: "El registro se actualizo con exito"
        })
        .catch(() => {
            response
                .status(400)
                .send({
                    mensaje: error.message || 'Algo salio mal con la base de datos'
                })
        })

}   