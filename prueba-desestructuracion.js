let nombres = ['pablo','aldana','nerea','bruno','david'];

let [alumno1, alumno2, ... alumnoResto] = nombres;

console.log(alumno1);
console.log(alumno2);
console.log(alumnoResto);

let casa = {
    habitaciones: {
        cantidad: 2,
        espacio:'60 mt2'
    },
    baños: 2,
    patio: false,
    cocina: true,
    escaleras: false
};

let { patio, cocina, escaleras} = casa;

console.log(patio);
console.log(cocina);
console.log(escaleras);

let {
    habitaciones: {
        cantidad: numero,
        espacio,
        baños = 0
    }
} = casa;
console.log(numero);
console.log(espacio);
console.log(baños);
