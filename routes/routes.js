import { Router } from "express"
import {createEmpresa, getAllEmpresa, getByIdEmpresa, updateByIdEmpresa} from "../controllers/empresa.controllers.js";
import { createSucursal, getAllSucursal, getByIdSucursal } from "../controllers/sucursal.controllers.js";

    const router = Router();
     
    router.post ('/crear/empresa', createEmpresa);
    router.get ('/empresa', getAllEmpresa);    
    router.get ('/empresa/id', getByIdEmpresa);
    router.put ('/empresa/update/:id', updateByIdEmpresa);
    router.get ('/sucursal', getAllSucursal);

    router.get ('/sucursal/id', getByIdSucursal);
    router.post ('/crear/sucursal', createSucursal);

    export default router;